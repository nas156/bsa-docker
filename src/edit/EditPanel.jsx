import React from 'react';
import * as actions from './editActions';
import { connect } from 'react-redux';
import { editMessage } from '../chat/chatActions';

class EditPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ''
        };
        this.onChange = this.onChange.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onApplyEdit = this.onApplyEdit.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.id !== '' && nextProps.id !== this.props.id) {
            const newMessage = this.props.list.find(message => message.id === nextProps.id);
            this.setState({
                message: newMessage.text
            });
        }
    }

    onChange(e) {
        const value = e.target.value;
        this.setState({
            message: value
        });
    }

    onApplyEdit(date) {
        this.props.editMessage(this.state.message, this.props.id, date);
        this.props.hideEdit();
        this.setState({
            message: ''
        });
        this.props.dropMessageId();
    }

    onCancel() {
        this.setState({
            message: ''
        });
        this.props.hideEdit();
        this.props.dropMessageId();
    }

    render() {
        return (
            this.props.isHidden ? null :
                <div className={'edit-panel'}>
                    <span>Edit message</span>
                    <textarea value={this.state.message} placeholder="Edit message" onChange={(e) => this.onChange(e)}></textarea>
                    <div>
                        <button className="edit" onClick={() => this.onApplyEdit(new Date().toISOString())}>edit</button>
                        <button className="cancel" onClick={() => this.onCancel()}>cancel</button>
                    </div>
                </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        list: state.chat.list,
        isHidden: state.edit.isHidden,
        id: state.edit.id
    }
}

const mapDispatchToProps = {
    ...actions,
    editMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPanel);