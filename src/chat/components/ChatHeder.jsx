import React from 'react';

class ChatHeader extends React.Component{
    render(){
        return(
            <div className='chat-header'>
                    <div>My chat</div>
                    <div>
                        {this.props.users} participants-
                        {this.props.online} online
                    </div>
                    <div>
                        {this.props.lastMessage} last message
                    </div>
            </div>
        );
    }
}

export default ChatHeader;
