import React from 'react';

class Message extends React.Component {

    handleLike(id) {
        this.props.onLike(id);
    }

    render() {
        return (
            <div className='message'>
                <img src={this.props.author} alt='' />
                <span className='msg' >{this.props.message}</span>
                <button className={this.props.liked} onClick={() => this.handleLike(this.props.id)}>🤍</button>
                <span className='time'>{this.props.time}</span>
            </div>
        );
    }
}

export default Message;