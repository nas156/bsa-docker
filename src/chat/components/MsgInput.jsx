import React from 'react';

class MsgInput extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            message: ''
        };
        this.cleanMsg = this.cleanMsg.bind(this);
    }

    onChange(e) {
        const value = e.target.value;
        this.setState({
            message: value
        });
    }

    cleanMsg() {
        document.getElementById('inputMessage').focus();
        this.setState({
            message: ''
        });
       
    }

    applySubmit(){
        this.props.onClick(this.state.message, new Date().toISOString());
        this.cleanMsg();
    }

    render() {
        return (
            <div className="msg-input">
                <textarea value={this.state.message} id = 'inputMessage'
                    onChange={(e) => this.onChange(e)} placeholder='Your message here' />
                <button type='submit' className="add-msg" onClick={() => this.applySubmit()}>
                    🢁 Send
                </button>
            </div>
        );
    }

}

export default MsgInput;