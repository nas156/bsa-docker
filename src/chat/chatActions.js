import { DELETE_MESSAGE, ADD_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE } from "./chatActionTypes"

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const addMessage = (text, date) => ({
    type: ADD_MESSAGE,
    payload: {
        text,
        date
    }
});

export const editMessage = (text, id, date) => ({
    type: EDIT_MESSAGE,
    payload: {
        text,
        id,
        date
    }
});

export const likeMessage = (id) => ({
    type: LIKE_MESSAGE,
    payload: {
        id
    }
});

