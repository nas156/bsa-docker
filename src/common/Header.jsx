import React from 'react';

class Header extends React.Component{
    
    render(){
        return(
            <div className = 'header'>
                <img src={ this.props.imageSrc } alt=''/>
            </div>
        );
    }

}

export default Header;