import {fetchProductsPending, fetchProductsSuccess} from '../chat/chatActions';

function fetchProducts() {
    return dispatch => {
        dispatch(fetchProductsPending());
        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
        .then(res => res.json())
        .then(res => {
            if(res.error) {
                throw(res.error);
            }
            dispatch(fetchProductsSuccess(res.products));
            return res.products;
        })
    }
}

export default fetchProducts;
